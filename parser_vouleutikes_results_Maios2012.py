#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
'''This file is part of ypes_parser.

    ypes_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ypes_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ypes_parser.  If not, see <http://www.gnu.org/licenses/>.
    '''

from results_parser import ResultsParser
from geodata_parser import EklogikesPeriferiesTopoJsonParser
from geodata_parser import DimoiTopoJsonParser

ROOT_DIR = "vouleutikes"
DIR_NAME = 'Maios_2012'

class VouleutikesResultsParser(ResultsParser):
    def __init__(self):
        self.dir = '/'.join([ROOT_DIR, DIR_NAME])
        ResultsParser.__init__(self, self.dir)
        self.base_url = 'http://ekloges-prev.singularlogic.eu/v2012a/dyn'

    def getDimous(self):
        self.get_results_per_dimo(process_before_saving=True, update_geodata=True)

        topojson_dir = "geodata"
        topojson_filename = "greece-dimoi-topo.json"
        topojson_parser = DimoiTopoJsonParser(in_topojson_file='/'.join([topojson_dir, topojson_filename]), out_topojson_file='/'.join([self.epikrateia_dir, topojson_filename]))
        topojson_parser.update_geodata_from_results_dir(self.dimoi_dir)

    def getEklogikesPeriferies(self):
        self.get_results_per_eklogiki_periferia(process_before_saving=True)

        topojson_dir = "geodata"
        topojson_filename = "eklogikes-periferies-topo.json"
        topojson_parser = EklogikesPeriferiesTopoJsonParser(in_topojson_file='/'.join([topojson_dir, topojson_filename]), out_topojson_file='/'.join([self.epikrateia_dir, topojson_filename]))
        topojson_parser.update_geodata_from_results_dir(self.eklogikes_periferies_dir)

    def getEpikrateia(self):
        self.get_resutls_for_epikrateia(process_before_saving=True)


if __name__ == '__main__':
    vouleutikesresults = VouleutikesResultsParser()
    vouleutikesresults.getDimous()
    vouleutikesresults.getEklogikesPeriferies()
    vouleutikesresults.getEpikrateia()


    # vouleutikesresults.get_results_per_party(process_before_saving=True)
    # στις βουλευτικες δεν δινουν αποτελεσματα ανα διοικητική περιφερεια
    # vouleutikesresults.get_results_per_dioikitiki_periferia(process_before_saving=True)