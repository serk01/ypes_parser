'''This file is part of ypes_parser.

    ypes_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ypes_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ypes_parser.  If not, see <http://www.gnu.org/licenses/>.
    '''

import urllib2
import simplejson


class NetworkOperations():
    def parse_url_to_json(self, url):
        retv = None
        try:
            req = urllib2.Request(url, None)
            opener = urllib2.build_opener()
            f = opener.open(req)
            retv = simplejson.load(f)
        except Exception as e:
            # print "failed to parse %s with error %s", (url, str(e))
            return None

        return retv
