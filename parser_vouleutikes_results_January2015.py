#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
'''This file is part of ypes_parser.

    ypes_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ypes_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ypes_parser.  If not, see <http://www.gnu.org/licenses/>.
    '''

from results_parser import ResultsParser

from geodata_parser import EklogikesPeriferiesTopoJsonParser
from geodata_parser import DimoiTopoJsonParser
import sys
ROOT_DIR = "vouleutikes"
DIR_NAME = 'January_2015'

class VouleutikesResultsParser(ResultsParser):
    def __init__(self, output_dir):
        self.dir = '/'.join([ROOT_DIR, DIR_NAME])
        ResultsParser.__init__(self, self.dir)
        # self.base_url = 'http://ekloges-prev.singularlogic.eu/v2012b/dyn'
        # self.base_url = 'http://ekloges.ypes.gr/current/dyn1/v'
        # self.results_dir = output_dir if len(output_dir) > 0 else '/home/anarresti/Projects/Radiobubble/rbdata/ekloges/results'
        self.results_dir = output_dir
        print("OUTPUT DIR:", self.results_dir)


    def getDimous(self):
        # self.base_url = 'http://ekloges.ypes.gr/current/dyn/v'
        self.base_url = 'http://ekloges-prev.singularlogic.eu/v2015a/dyn/v'
        self.get_results_per_dimo(process_before_saving=True, update_geodata=True)

        topojson_dir = "geodata"
        topojson_filename = "greece-dimoi-topo.json"
        topojson_parser = DimoiTopoJsonParser(in_topojson_file='/'.join([topojson_dir, topojson_filename]), out_topojson_file='/'.join([self.epikrateia_dir, topojson_filename]))
        topojson_parser.update_geodata_from_results_dir(self.dimoi_dir)

    def getEklogikesPeriferies(self):
        self.base_url = 'http://ekloges-prev.singularlogic.eu/v2015a/dyn/v'
        self.get_results_per_eklogiki_periferia(process_before_saving=True)

        topojson_dir = "geodata"
        topojson_filename = "eklogikes-periferies-topo.json"
        topojson_parser = EklogikesPeriferiesTopoJsonParser(in_topojson_file='/'.join([topojson_dir, topojson_filename]), out_topojson_file='/'.join([self.epikrateia_dir, topojson_filename]))
        topojson_parser.update_geodata_from_results_dir(self.eklogikes_periferies_dir)

    def getEpikrateia(self):
        self.base_url = 'http://ekloges-prev.singularlogic.eu/v2015a/dyn1/v'
        self.get_resutls_for_epikrateia(process_before_saving=True)


if __name__ == '__main__':
    output_dir_path = ""
    if len(sys.argv) > 1:
        output_dir_path = sys.argv[1]
        vouleutikesresults = VouleutikesResultsParser(output_dir=output_dir_path)
        vouleutikesresults.getDimous()
        vouleutikesresults.getEklogikesPeriferies()
        vouleutikesresults.getEpikrateia()
    else:
        print("no output dir specified")


    # vouleutikesresults.get_results_per_party(process_before_saving=True)
    # στις βουλευτικες δεν δινουν αποτελεσματα ανα διοικητική περιφερεια
    # vouleutikesresults.get_results_per_dioikitiki_periferia(process_before_saving=True)