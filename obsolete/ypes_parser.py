
# -*- coding: utf-8 -*-

from csv_to_dict import Csv2Dict
from network_operations import NetworkOperations
import simplejson
import os
from results_parser import ResultsParser

'''
Parses ekloges.ypes.gr for European election results

The url structure and the response objects seem to be different for
the 2014 European elections compared to previous European elections.

This script only supports 2014 results at the moment
'''


RESULT_TYPE = ['dimoi', 'eklogikes_periferies', 'kommata', 'dioikitikes_periferies']

# http://ekloges-prev.singularlogic.eu/

class YpesParser():
    def __init__(self):
        self.base_url = "http://ekloges.ypes.gr"
        self.base_url = "http://ekloges.ypes.gr/may2014/dyn/e/"
        self.base_url = "http://ekloges-prev.singularlogic.eu/may2014/dyn/e/"
        self.eklogikes_periferies_prefix = "ep_"
        self.dioikitikes_perifereies_prefix = "snom_"
        self.komma_prefix = "party_"
        self.dimoi_prefix = "dhm_"
        self.extension = ".js"

        # self.exports_dir = os.path.join(root_dir, 'private', 'exports/')
        self.exports_dir = 'exports'
        self.dimoi_dir = self.exports_dir + '/dimoi_json'
        self.eklogikes_periferies_dir = self.exports_dir + '/eklogikes_periferies_json'
        self.kommata_dir = self.exports_dir + '/kommata_json'
        self.dioikitikes_perifereies_dir = self.exports_dir + '/dioikities_periferies_json'

        self.file2dict = Csv2Dict()
        self.json_from_url = NetworkOperations()

    def generate_url_for_key_and_prefix(self, key, prefix):
        return self.base_url + prefix + key + self.extension

    def parse_dioikitikes_periferies(self):
        dioikitikes_periferies = self.file2dict.dictify_dioikitikes_periferies()
        print dioikitikes_periferies.keys()
        for k in dioikitikes_periferies.keys():
            url = self.generate_url_for_key_and_prefix(k, self.dioikitikes_perifereies_prefix)
            print url
            filename = dioikitikes_periferies[k]
            json = self.json_from_url.parse_url_to_json(url)
            if json:
                self.process_json_and_save(json, filename, self.dioikitikes_perifereies_dir, 'party', 'PARTY_ID')

    def parse_eklogikes_periferies(self):
        eklogikes_periferies = self.file2dict.dictify_eklogikes_periferies()
        print eklogikes_periferies.keys()
        for k in eklogikes_periferies.keys():
            url = self.generate_url_for_key_and_prefix(k, self.eklogikes_periferies_prefix)
            print url
            filename = eklogikes_periferies[k]
            json = self.json_from_url.parse_url_to_json(url)
            if json:
                return self.process_json_and_save(json, filename, self.eklogikes_periferies_dir, 'party', 'PARTY_ID')



    def parse_komata(self):
        kommata = self.file2dict.dictify_kommata()
        print kommata.keys()
        for k in kommata.keys():
            url = self.generate_url_for_key_and_prefix(k, self.komma_prefix)
            print url
            filename = kommata[k]
            json = self.json_from_url.parse_url_to_json(url)
            if json:
                # self.format_partydata_and_save(json, filename, self.kommata_dir, 'ep', 'EP_ID')
                self.save_json_result(json, filename, self.kommata_dir)



    def testobject(self, obj):
        print

    def parse_dimous(self):
        dimoi = self.file2dict.dictify_dimoi()
        print dimoi.keys()
        for k in dimoi.keys():
            url = self.generate_url_for_key_and_prefix(k, self.dimoi_prefix)
            print url
            filename = dimoi[k]
            print filename
            json = self.json_from_url.parse_url_to_json(url)
            if json:
                return self.process_json_and_save(json, filename, self.dimoi_dir, 'party', 'PARTY_ID')


    def save_json_result(self, json, filename, dir):
        filename += '.json'
        try:
            if not os.path.exists(dir):
                os.makedirs(dir)
            with open(dir + "/" + filename, 'w') as outfile:
                simplejson.dump(json, outfile)
                print 'saved file:', outfile
                print json
                return json
        except  Exception as e:
            print 'failed saving ' + filename + " - " + str(e)

    '''
    parses the results and create a new dictionary with the obj_key so that
    we can get each party's results directly from the dict instead of having to
    loop through the original array of results
    '''
    def process_json_and_save(self, json, filename, dir, array_key, obj_key):
        d = {}
        array = json[array_key]
        for object in array:
            d[object[obj_key]] = object
            # print object['EP_ID']
        # replace json's array with new key-value pairs
        json[array_key] = d
        # print json['ep']

        return self.save_json_result(json, filename, dir)



if __name__ == '__main__':
    parser = YpesParser()
    parser.parse_komata()
    # parser.parse_dimous()
    # parser.parse_eklogikes_periferies()
    # parser.parse_dioikitikes_periferies()