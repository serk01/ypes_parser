from obsolete.ypes_parser import YpesParser


'''Supporting EU elections 2014 and wip 2009'''


''' 2014 παραδειγματα

Επικρατεια - http://ekloges.ypes.gr/may2014/dyn/e/epik_1.js
Εκλογικη περιφερεια Α'Αθηνων - http://ekloges.ypes.gr/may2014/dyn/e/ep_38.js
Εκλογικη Περιφερεια Β'Αθηνων, Δημος Αγ. Βαρβαρας - http://ekloges.ypes.gr/may2014/dyn/e/dhm_9179.js
Συριζα - http://ekloges.ypes.gr/may2014/dyn/e/party_4.js
Διοικητικη περιφερεια Στερεας ελλαδα - http://ekloges.ypes.gr/may2014/dyn/e/snom_8.js

'''

'''2009 παραδειγματα
εκλογικη περιφερεια - http://ekloges-prev.singularlogic.eu/e2009/dyn/ep_45.js
'''

''' Απο το 2009 και πισω τα responses δεν ειναι σε καθαρη JSON γαμω τον χριστουλη τους
Και απο το 2004 οι σελιδες ειναι εντελως διαφορετικες οποτε δεν μπορουν να υποστηριχθουν προς το παρων'''

base_urls = {'2014': 'http://ekloges.ypes.gr/may2014/dyn/e',
             '2009': 'http://ekloges-prev.singularlogic.eu/e2009/dyn'}

class YpesParserEU(YpesParser):
    def __init__(self, year):
        super(YpesParserEU, self).__init__()

        self.base_url = base_urls[year]
        self.eklogikes_periferies_prefix = "ep_"
        self.dioikitikes_perifereies_prefix = "snom_" # 2009 doesn't provide results for eklogikes periferies
        self.komma_prefix = "party_"
        self.dimoi_prefix = "dhm_"
        self.extension = ".js"
