import unittest

from obsolete.ypes_parser import YpesParser


class YpesParserTest(unittest.TestCase):

    def setUp(self):
        self.ypesparser = YpesParser()

    def test_parse_dimous_is_not_empty(self):
        result = self.ypesparser.parse_dimous()
        self.assertTrue(len(result) > 0, "result is empty")

    def test_parse_eklogikes_periferies_is_not_empty(self):
        result = self.ypesparser.parse_eklogikes_periferies()
        self.assertTrue(len(result) > 0, "result is empty")

if __name__ == "__main__":
    unittest.main()
