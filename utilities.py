'''This file is part of ypes_parser.

    ypes_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ypes_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ypes_parser.  If not, see <http://www.gnu.org/licenses/>.
    '''
from __future__ import division

import simplejson
import os
import json

def save_json_result(json, filename, dir):
    filename += '.json'
    try:
        if not os.path.exists(dir):
            os.makedirs(dir)
        with open(dir + "/" + filename, 'w') as outfile:
            simplejson.dump(json, outfile)
            print 'saved file:', filename
            print json
            return json
    except  Exception as e:
        print 'failed saving ' + filename + " - " + str(e)

'''
parses the results and create a new dictionary with the obj_key so that
we can get each party's results directly from the dict instead of having to
loop through the original array of results

:param json the incoming json file
:dataID the id of the incoming data as matched from the the proper area coding file
:array_key the key to be used in the reformated json object
:obj_key the object key to look for in the incoming json file
:name the name of the refering object (party, eklogiki periferia etc)
'''

# The municipality data doesn't include the percentage of each party,
# We add it here so we don't have to do it on the fly in the browser
def compute_percentage_for_municipality(json):
    for party in json['party']:
        try:
            egira = json['Egkyra']
            votes = party['VOTES']
            party['Perc'] = ( votes / egira) * 100
        except:
            print("couldn't compute percentage")

def format_partydata_and_save(json, dataID, dir, array_key, obj_key, name):
    parties_dicts = {}
    parties_array = json[array_key]
    maxvotes = 0
    maxvotes_partyId = 0

    for party in parties_array:
        party_code = party[obj_key]
        parties_dicts[party_code] = party
        number_of_votes = party['VOTES']
        if number_of_votes > maxvotes:
            maxvotes = number_of_votes
            maxvotes_partyId = party_code

    # replace json's array with new key-value pairs
    # json[array_key] = new_array
    json[array_key+"_dict"] = parties_dicts
    json['name'] = name
    json['top_party_id'] = maxvotes_partyId

    # add a 0 in front of single digit IDs to match the data in the geojson files we have
    try:
        if int(dataID) < 10:
            dataID = "0"+dataID

        # if update_geodata:
        #     topParser.topojson_parser(dataID, maxvotes_partyId)
    except:
        print("data ID incopatible")

    return save_json_result(json, dataID, dir)


# def saveTopojson():
#     topParser.save()
#
# class TopoJSONParser:
#     def __init__(self):
#         # self.file_in = "geodata/eklogikes-periferies-topo.json"
#         # self.file_out = "geodata/eklogikes-periferies-updated-topo.json"
#         self.file_in = "geodata/greece-dimoi-min.topo.json"
#         self.file_out = "geodata/greece-dimoi-min-updated.topo.json"
#         json_file = open(self.file_in)
#
#         self.json_data = json.load(json_file)
#         json_file.close()
#
#
#     def topojson_parser(self, dataID, top_party_id):
#         name = 'greece-dimoi'
#         geometries = self.json_data['objects'][name]['geometries']
#         for geometry in geometries:
#             if geometry['properties']['KWD_YPES'] == dataID:
#                 print("found")
#                 geometry['properties']['top_party_id'] = top_party_id
#
#     def save(self):
#         with open(self.file_out, 'w') as outfile:
#             json.dump(self.json_data, outfile)
#
#
# topParser = TopoJSONParser()