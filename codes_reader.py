'''This file is part of ypes_parser.

    ypes_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ypes_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ypes_parser.  If not, see <http://www.gnu.org/licenses/>.
    '''
import csv
import os


class CodeReader:
    def __init__(self):
        self.kommata = []
        self.eklogikes_periferies = []
        self.dioikitikes_periferies = []
        self.dimoi = []


        self.csv_dir = 'area_codings/'


    def dictify_dimoi(self):
        file = self.csv_dir + 'dimoi.tsv'
        self.dimoi = self.csv_read(file)
        return self.dimoi


    def dictify_dioikitikes_periferies(self):
        file = self.csv_dir + 'dioikitikes_periferies.tsv'
        self.dioikitikes_periferies = self.csv_read(file)
        return self.dioikitikes_periferies

    def dictify_eklogikes_periferies(self):
        file = self.csv_dir + 'eklogikes_periferies.tsv'
        self.eklogikes_periferies = self.csv_read(file)
        return self.eklogikes_periferies


    def dictify_kommata(self):
        file = self.csv_dir + 'kommata.tsv'
        self.kommata = self.csv_read(file)
        return self.kommata


    def csv_read(self, file):
        with open(file) as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            dictionary = dict([key, value] for key, value in reader)

        return dictionary


if __name__ == '__main__':
    c = CodeReader()
    c.dictify_files()
    print c.dimoi['9179']



