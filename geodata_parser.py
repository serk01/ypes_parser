'''This file is part of ypes_parser.

    ypes_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ypes_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ypes_parser.  If not, see <http://www.gnu.org/licenses/>.
    '''
import json
import os

'''

'''

class TopoJSONParser:
    def __init__(self, in_topojson_file, out_topojson_file):

        self.key_name = "" # overwritten by specific implementations

        self.json_file = open(in_topojson_file)
        self.json_data = json.load(self.json_file)
        self.json_file.close()
        self.file_out = out_topojson_file


    def update_geodata_from_results_dir(self, results_dir):
        print(results_dir)
        results_list = os.listdir(results_dir)
        for file in results_list:
            area_id = file.split('.')[0]
            area_data = json.load(open('/'.join([results_dir, file]), 'r'))
            top_part_id_for_area = area_data['top_party_id']
            self.topojson_parser(area_id, top_part_id_for_area)

        self.save()

    def save(self):
        with open(self.file_out, 'w+') as outfile:
            json.dump(self.json_data, outfile)


'''
adds the top_party_id to the each eklogiki periferias topojson object
so that we can load the proper colour in our maps at the same time
instead of having to read each eklogiki periferias json data to get the
top_party_id for that area
'''
class EklogikesPeriferiesTopoJsonParser(TopoJSONParser):
    def __init__(self, in_topojson_file, out_topojson_file):
        TopoJSONParser.__init__(self, in_topojson_file, out_topojson_file)
        # this refers to the field inside the topojson file, that identifies the root of
        # the geometries we need to update
        self.key_name = "eklogikes-periferies"

    def topojson_parser(self, area_id, top_party_id):
        geometries = self.json_data['objects'][self.key_name]['geometries']
        for geometry in geometries:
            if geometry['properties']['CCODE'] == area_id:
                print("update topojson file for eklogiki periferia %s" %area_id)
                geometry['properties']['top_party_id'] = top_party_id


class DimoiTopoJsonParser(TopoJSONParser):
    def __init__(self, in_topojson_file, out_topojson_file):
        TopoJSONParser.__init__(self, in_topojson_file, out_topojson_file)

        # this refers to the field inside the topojson file, that identifies the root of
        # the geometries we need to update
        self.key_name = "dimoi"

    def topojson_parser(self, area_id, top_party_id):
        geometries = self.json_data['objects'][self.key_name]['geometries']
        for geometry in geometries:
            if geometry['properties']['KWD_YPES'] == area_id:
                print("update topojson file for dimo %s" %area_id)
                geometry['properties']['top_party_id'] = top_party_id
