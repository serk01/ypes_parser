'''This file is part of ypes_parser.

    ypes_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ypes_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ypes_parser.  If not, see <http://www.gnu.org/licenses/>.
    '''
from codes_reader import CodeReader
import network_operations
import utilities

class ResultsParser:
    def __init__(self, dirname):
        self.base_url = "" # overwritten in children class
        self.dirname = dirname
        self.eklogikes_periferies_prefix = "ep_"
        self.dioikitikes_perifereies_prefix = "snom_"
        self.komma_prefix = "party_"
        self.dimoi_prefix = "dhm_"
        self.epikrateia_prefix = "epik_1"
        self.extension = ".js"
        self.dimos_url = ""


        # directories
        self.results_dir = 'results'
        # self.epikrateia_dir = '/'.join([self.results_dir, dirname])
        # self.dimoi_dir = '/'.join([self.results_dir, dirname, 'dimoi_json'])
        # self.eklogikes_periferies_dir = '/'.join([self.results_dir, dirname, 'eklogikes_periferies_json'])
        # self.kommata_dir = '/'.join([self.results_dir, dirname, 'kommata_json'])
        # self.dioikitikes_perifereies_dir = '/'.join([self.results_dir, dirname, 'dioikities_periferies_json'])

        # post-processing keys used to change final json structure
        self.dimos_array_key_str = "party"
        self.dimos_party_object_key_str = "PARTY_ID"
        self.kommata_array_key_str = 'ep'
        self.kommata_object_key_str = 'EP_ID'
        self.ep_array_key_str = 'party'
        self.ep_object_key_str = 'PARTY_ID'
        self.snom_array_key_str = 'party'
        self.snom_object_key_str = 'PARTY_ID'


        self.codes_reader = CodeReader()
        self.netoperations = network_operations.NetworkOperations()

    def set_directories(self):
        self.epikrateia_dir = '/'.join([self.results_dir, self.dirname])
        self.dimoi_dir = '/'.join([self.results_dir, self.dirname, 'dimoi_json'])
        self.eklogikes_periferies_dir = '/'.join([self.results_dir, self.dirname, 'eklogikes_periferies_json'])
        self.kommata_dir = '/'.join([self.results_dir, self.dirname, 'kommata_json'])
        self.dioikitikes_perifereies_dir = '/'.join([self.results_dir, self.dirname, 'dioikities_periferies_json'])

    def generate_url_for_key_and_prefix(self, prefix):
        return '/'.join([self.base_url, prefix])

    def get_resutls_for_epikrateia(self, process_before_saving):
        self.set_directories()

        url = self.generate_url_for_key_and_prefix(''.join([self.epikrateia_prefix, self.extension]))
        print("requesting %s" %url)
        json = self.netoperations.parse_url_to_json(url)
        if json:
            if process_before_saving:
                utilities.format_partydata_and_save(json, "epikrateia", self.epikrateia_dir, self.dimos_array_key_str, self.ep_object_key_str, "")
            else:
                utilities.save_json_result(json, "epikrateia_unprocessed", self.epikrateia_dir)
        else:
            print("failed to get results for %s" %url)

    def get_results_per_dimo(self, process_before_saving, update_geodata):
        self.set_directories()

        dimoi_codes = self.codes_reader.dictify_dimoi()
        print dimoi_codes.keys()

        for dimosID in dimoi_codes.keys():
            print
            url = self.generate_url_for_key_and_prefix(''.join([self.dimoi_prefix, dimosID, self.extension]))
            print("requesting %s" %url)
            dimos = dimoi_codes[dimosID]
            json = self.netoperations.parse_url_to_json(url)
            if json:
                if process_before_saving:
                    utilities.compute_percentage_for_municipality(json)
                    utilities.format_partydata_and_save(json, dimosID, self.dimoi_dir, self.dimos_array_key_str, self.dimos_party_object_key_str, dimos)
                else:
                    utilities.save_json_result(json[self.dimos_array_key_str], file, self.dimoi_dir)
            else:
                print("failed to get results for %s" %dimos)


    def get_results_per_party(self, process_before_saving):
        self.set_directories()

        kommata_codes = self.codes_reader.dictify_kommata()
        print kommata_codes.keys()
        for kommaID in kommata_codes.keys():
            print
            url = self.generate_url_for_key_and_prefix(''.join([self.komma_prefix, kommaID, self.extension]))
            print("requesting %s" %url)
            komma = kommata_codes[kommaID]
            json = self.netoperations.parse_url_to_json(url)
            if json:
                if process_before_saving:
                    utilities.format_partydata_and_save(json, kommaID, self.kommata_dir, self.kommata_array_key_str, self.kommata_object_key_str, komma)
                    return
                else:
                    utilities.save_json_result(json, komma, self.kommata_dir)
            else:
                print("failed to get results for %s" %komma)

    def get_results_per_eklogiki_periferia(self, process_before_saving):
        self.set_directories()

        eklogikes_periferies_codes = self.codes_reader.dictify_eklogikes_periferies()
        print eklogikes_periferies_codes.keys()
        for eklogiki_periferia_id in eklogikes_periferies_codes.keys():
            print
            url = self.generate_url_for_key_and_prefix(''.join([self.eklogikes_periferies_prefix, eklogiki_periferia_id, self.extension]))
            print("requesting %s" %url)
            eklogiki_periferia = eklogikes_periferies_codes[eklogiki_periferia_id]
            json = self.netoperations.parse_url_to_json(url)
            if json:
                if process_before_saving:
                    utilities.format_partydata_and_save(json, eklogiki_periferia_id, self.eklogikes_periferies_dir, self.ep_array_key_str, self.ep_object_key_str, eklogiki_periferia)
                else:
                    utilities.save_json_result(json, eklogiki_periferia, self.eklogikes_periferies_dir)
            else:
                print("failed to get results for %s" %eklogiki_periferia)

    def get_results_per_dioikitiki_periferia(self, process_before_saving):
        self.set_directories()

        dioikitikes_periferies = self.codes_reader.dictify_dioikitikes_periferies()
        print dioikitikes_periferies.keys()
        for k in dioikitikes_periferies.keys():
            url = self.generate_url_for_key_and_prefix(''.join([self.dioikitikes_perifereies_prefix, k, self.extension]))
            print url
            filename = dioikitikes_periferies[k]
            json = self.netoperations.parse_url_to_json(url)
            if json:
                if process_before_saving:
                    utilities.format_partydata_and_save(json, filename, self.dioikitikes_perifereies_dir, self.snom_array_key_str, self.snom_object_key_str)
                else:
                    utilities.save_json_result(json, filename, self.dioikitikes_perifereies_dir)