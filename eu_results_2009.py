'''This file is part of ypes_parser.

    ypes_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ypes_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ypes_parser.  If not, see <http://www.gnu.org/licenses/>.
    '''
from results_parser import ResultsParser

'''
        gia dimous - ekloges-prev.singularlogic.eu/e2009/dyndat/1/dhm_4005.js

        kai einai kai oi kwdikoi diaforetikoi????
'''
class EUResultsParser2009(ResultsParser):
    def __init__(self):
        ResultsParser.__init__(self, '2009')
        self.base_url = 'http://ekloges.ypes.gr/may2014/dyn/e'
        self.base_url = 'http://ekloges-prev.singularlogic.eu/e2009/dyn'



        self.dimos_array_key_str = "vardhm2"
        self.dimos_party_object_key_str = "party_id"
        # self.ypesparser = YpesParser()

        self.run()

    # def run(self):
    #     self.ypesparser.testobject(self)



if __name__ == '__main__':
    euresults2009 = EUResultsParser2009()
    euresults2009.get_results_per_dimo(process_to_dict=True)
