'''This file is part of ypes_parser.

    ypes_parser is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ypes_parser is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ypes_parser.  If not, see <http://www.gnu.org/licenses/>.
    '''
from results_parser import ResultsParser


ROOT_DIR = "eu_results"
DIR_NAME = '2014'

class EUResultsParser2014(ResultsParser):
    def __init__(self):
        self.dir = '/'.join([ROOT_DIR, DIR_NAME])
        ResultsParser.__init__(self, self.dir)
        self.base_url = 'http://ekloges.ypes.gr/may2014/dyn/e'

if __name__ == '__main__':
    euresults2014 = EUResultsParser2014()
    euresults2014.get_results_per_dimo(process_before_saving=True)
    euresults2014.get_results_per_party(process_before_saving=True)
    euresults2014.get_results_per_eklogiki_periferia(process_before_saving=True)
    euresults2014.get_results_per_dioikitiki_periferia(process_before_saving=True)